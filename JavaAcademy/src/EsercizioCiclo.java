import java.util.Scanner;

public class EsercizioCiclo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner tastiera = new Scanner(System.in);

		System.out.print("Inserisci un numero : ");
		int numero = tastiera.nextInt();

		int quanti = 0;
		int i = 1;

		while (i <= numero) {
			if (numero % i == 0) {
				System.out.println(i);
				quanti++;
			}
			i++;

			if (quanti > 2) {
				System.out.print("Il numero ha più di 2 divesori : ");
			} else {
				System.out.print("Il numero  non ha più di 2 divesori : ");
			}

		}

		System.out.println("num totale dei divisori : " + quanti);

		tastiera.close();

	}

}
