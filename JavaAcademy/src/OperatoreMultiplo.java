import java.util.Scanner;

public class OperatoreMultiplo {
	public static void main(String[] args) {
		
		Scanner tastiera = new Scanner(System.in);

		int numero;

		System.out.println("inserisci un numero");

		numero = tastiera.nextInt();

		if (numero >= 10 && numero % 2 == 0) {
			System.out.println("il numero e maggione di 10 e pari");
		} else {
			System.out.println("il numero e dispari");
		}

		tastiera.close();
	}
}
