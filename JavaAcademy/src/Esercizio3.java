import java.util.Scanner;

public class Esercizio3 {
	public static void main(String[] args) {
		Scanner tastiera = new Scanner(System.in);

		int numero1, numero2;

		System.out.println("inserisci il primo numero");
		numero1 = tastiera.nextInt();

		System.out.println("inserisci il secondo numero");
		numero2 = tastiera.nextInt();

		if (numero1 % numero2 == 0) {
			System.out.println(" si");
		} else {
			System.out.println(" no ");
		}

		tastiera.close();
	}
}
