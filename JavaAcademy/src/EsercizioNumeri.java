import java.util.Scanner;

public class EsercizioNumeri {
	public static void main(String[] args) {
		Scanner tastiera = new Scanner(System.in);

		int numero1, numero2;

		System.out.println(" Inserisci il primo numero ");
		numero1 = tastiera.nextInt();

		System.out.println("Inseriscri il secondo numero ");
		numero2 = tastiera.nextInt();

		if (numero1 == numero2) {
			System.out.println(" i numeri sono uguali ");
		} else {
			System.out.println(" i numeri sono diversi ");
		}

		tastiera.close();
	}
}
